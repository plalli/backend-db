#!/usr/bin/env python

'''
  Back end DB server for Assignment 3, CMPT 474.
'''

# Library packages
import argparse
import json
import sys
import time

# Installed packages
import boto.dynamodb2
import boto.dynamodb2.table
import boto.sqs

from boto.dynamodb2.exceptions import ItemNotFound
from boto.dynamodb2.items import Item

AWS_REGION = "us-west-2"
TABLE_NAME_BASE = "activities"
Q_IN_NAME_BASE = "a3_back_in"
Q_OUT_NAME = "a3_out"

MAX_TIME_S = 3600 # One hour
MAX_WAIT_S = 20 # SQS sets max. of 20 s
DEFAULT_VIS_TIMEOUT_S = 60

from boto.dynamodb2.table import Table

def handle_args():
    argp = argparse.ArgumentParser(description="Backend for simple database")
    argp.add_argument('suffix', help="Suffix for queue base ({0}) and table base ({1})".format(Q_IN_NAME_BASE, TABLE_NAME_BASE))
    return argp.parse_args()

def create_user(table, args):
    try:
        item = table.get_item(id = args["id"], consistent=True)
        if item["name"] != args["name"]:
            return {
                "errors": [{
                    "id_exists": {
                        "status": "400",  # "Bad Request"
                        "title": "id already exists",
                        "detail": {"name": item['name']}
                    }
                }],
                "statusCode": 400
            }

    except ItemNotFound as inf:
        p = Item(table, data={'id': args["id"], 'name': args["name"], 'activities': set()})
        p.save()

    return {
        "data": {
            "type": "person",
            "id": args["id"],
            "links": args["links"]
        },
        "statusCode": 201
    }

def add_activity(table, args):
    success = {
                "data": {
                    "type": "person",
                    "id": args["id"],
                    "added": []
                },
                "statusCode": 200
            }

    try:
        item = table.get_item(id=args["id"], consistent=True)
        activities = item["activities"]
        activityArg = args["activity"]
        addedActivity = None

        if activities == None: #if activities list is empty
            activities = [activityArg]
            success["data"]["added"].append(activityArg)
            addedActivity = activityArg
        elif activityArg not in activities: #if activities is not empty, and does not contain activity being added
            activities.append(activityArg)
            success["data"]["added"].append(activityArg)
            addedActivity = activityArg

        if addedActivity != None: #if an activity was added, then save, otherwise skip
            item["activities"] = activities
            item.partial_save()

        return success

    except ItemNotFound as inf:
        return {
            "errors": [{
                "not_found": {
                    "id": args["id"]
                }
            }],
            "statusCode": 404
        }

def delete_user_by_id(table, args):
    try:
        item = table.get_item(id = args["id"], consistent=True)
        table.delete_item(id = args["id"])

        return {
            "data": {
                "type": "person",
                "id": args["id"],
            },
            "statusCode": 200
        }
    except ItemNotFound as inf:
        return {
            "errors": [{
                "not_found": {
                    "id": args["id"]
                }
            }],
            "statusCode": 404
        }

def delete_activity(table, args):
    try:
        item = table.get_item(id = args["id"], consistent=True)
        activities = item["activities"]
        activity = args["activity"]

        # if the activities list is not empty, and the activity exists.
        if activities != None and activity in activities:
            activities.remove(activity)

            item.partial_save()
            return {
                "data": {
                    "type": "person",
                    "id": args["id"],
                    "deleted": [activity]
                },
                "statusCode": 200
            }
        else: #activity list is empty or activity doesnt exist
            return {
                "data": {
                    "type": "person",
                    "id": args["id"],
                    "deleted": []
                },
                "statusCode": 200
            }

    except ItemNotFound as inf:
        return {
            "errors": [{
                "not_found": {
                    "id": args["id"]
                }
            }],
            "statusCode": 404
        }

def get_user_by_name(table, args):
        results = table.scan()
        for item in results:
            if item["name"] == args["name"]:
                return {
                    "data": {
                        "type": "person",
                                "id": int(item["id"]),
                                "name": args["name"],
                                "activities": [] if item["activities"] is None else item["activities"]
                    },
                    "statusCode": 200
                }

        return {
            "errors": [{
                "not_found": {
                    "name": args["name"]
                }
            }],
            "statusCode": 404
        }

def get_user_by_id(table, args):
    try:
        item = table.get_item(id=args["id"], consistent=True)
    except ItemNotFound as inf:
        return {
            "errors": [{
                "not_found": {"id": args["id"]}
            }],
            "statusCode": 404
        }

    return {
        "data": {
            "type": "person",
            "id": args["id"],
            "name": item["name"],
            "activities": [] if item["activities"] is None else item["activities"]
        },
        "statusCode": 200
    }

def get_all_users(table):
    items = table.scan()

    jsonData = {
        "data": [],
        "statusCode": 200
    }

    for item in items:
        itemId = item["id"]
        jsonData["data"].append({"id": int(itemId), "type": "users"})

    return jsonData

def delete_by_name(table, args):
    items = table.scan(name__eq=args["name"])

    for item in items:
        if item["name"] == args["name"]:
            id = item["id"]
            item.delete()
            return {
                "data": {
                    "type": "person",
                    "id": int(id)
                },
                "statusCode": 200
            }
    else:
        return {
            "errors": [{
                "not_found": {
                    "name": args["name"]
                }
            }],
            "statusCode": 404
        }

if __name__ == "__main__":
    args = handle_args()
    '''
       EXTEND:

       After the above statement, args.suffix holds the suffix to use
       for the input queue and the DynamoDB table.

       This main routine must be extended to:
       1. Connect to the appropriate queues
       2. Open the appropriate table
       3. Go into an infinite loop that
          - reads a requeat from the SQS queue, if available
          - handles the request idempotently if it is a duplicate
          - if this is the first time for this request
            * do the requested database operation
            * record the message id and response
            * put the response on the output queue
    '''

    #1. Connect to the appropriate queues
    conn = boto.sqs.connect_to_region(AWS_REGION)
    q_in = conn.create_queue(Q_IN_NAME_BASE + args.suffix)
    q_out = conn.create_queue(Q_OUT_NAME)
    #2 Open the appropriate table
    table = Table(TABLE_NAME_BASE, connection=boto.dynamodb2.connect_to_region(AWS_REGION))

    wait_start = time.time()

    responseDict = {}
    loadedMsgsDict = {}

    #3 Go into an infinite loop
    currentOpNum = 1

    while True:
        #Only proceed if opnum is correct and passed into dictionary

        if currentOpNum in loadedMsgsDict:
            body = loadedMsgsDict[currentOpNum]
            msg_id = body["msg_id"]
            msg_op = body["op"]
            msg_args = body["args"]
            currentOpNum += 1

            #if msg is in dictionary, return the previous response back
            if msg_id in responseDict:
                print "Found msg in dictionary"
                return_msg = responseDict[msg_id]
                return_msg['msg_id'] = msg_id
                return_msg_json = json.dumps(return_msg)
                msg = boto.sqs.message.Message()
                msg.set_body(return_msg_json)
                q_out.write(msg)

            #Do the requested database operation
            else:
                returnBodyMsg = {}
                if(msg_op == 'create_user'):
                    returnBodyMsg = create_user(table, msg_args)
                elif(msg_op == 'add_activity'):
                    returnBodyMsg = add_activity(table, msg_args)
                elif(msg_op == 'delete_activity'):
                    returnBodyMsg = delete_activity(table, msg_args)
                elif(msg_op == 'get_user_by_id'):
                    returnBodyMsg = get_user_by_id(table, msg_args)
                elif(msg_op == 'get_all_users'):
                    returnBodyMsg = get_all_users(table)
                elif(msg_op == 'get_user_by_name'):
                    returnBodyMsg = get_user_by_name(table, msg_args)
                elif(msg_op == 'delete_user_by_id'):
                    returnBodyMsg = delete_user_by_id(table, msg_args)
                elif(msg_op == 'delete_by_name'):
                    returnBodyMsg = delete_by_name(table, msg_args)

                #Record the message id and response
                returnBodyMsg['msg_id'] = msg_id
                responseDict[msg_id] = returnBodyMsg

                #Put the response on the output queue
                return_msg_json = json.dumps(returnBodyMsg)

                msg = boto.sqs.message.Message()
                msg.set_body(return_msg_json)
                q_out.write(msg)

        #Read a request from SQS queue, if available
        msg_in = q_in.read(wait_time_seconds=MAX_WAIT_S, visibility_timeout=DEFAULT_VIS_TIMEOUT_S)
        #If duplicate, handle request idempotently
        if msg_in:
            body = json.loads(msg_in.get_body())
            #Save incoming messages to dictionary
            msg_opnum = body["opnum"]
            loadedMsgsDict[msg_opnum] = body
            q_in.delete_message(msg_in)

            wait_start = time.time()
        elif time.time() - wait_start > MAX_TIME_S:
            print "\nNo messages on input queue for {0} seconds. Server no longer reading response queue {1}.".format(MAX_TIME_S, q_out.name)
            break
        else:
            pass
