'''
   Ops for the frontend of Assignment 3, Summer 2016 CMPT 474.
'''

# Standard library packages
import json

# Installed packages
import boto.sqs

# Imports of unqualified names
from bottle import post, get, put, delete, request, response

# Local modules
import SendMsg

# Constants
AWS_REGION = "us-west-2"

Q_IN_NAME_BASE = 'a3_in'
Q_OUT_NAME = 'a3_out'

# Global Queue Op
def setup_op_counter():
    global seq_num
    zkcl = send_msg_ob.get_zkcl()
    if not zkcl.exists('/SeqNum'):
        zkcl.create('/SeqNum', "0")
    else:
        zkcl.set('/SeqNum', "0")

    seq_num = zkcl.Counter('/SeqNum')

# Respond to health check
@get('/')
def health_check():
    response.status = 200
    return "Healthy"

'''
# EXTEND:
# Define all the other REST operations here ...
@post('/users')
def create_route():
    pass
'''
OP = 'op' #operation being performed
ARGS = 'args' #any arguments for that

#converts a dictionary to a json string and sends it to both queues.
def convert_and_send(msgDict):
    global seq_num
    seq_num += 1

    v_local = seq_num.last_set

    print "Sending op num: {0}".format(v_local)

    msgDict["opnum"] = v_local

    json_str = json.dumps(msgDict)
    msg_a = boto.sqs.message.Message()
    msg_a.set_body(json_str)

    msg_b = boto.sqs.message.Message()
    msg_b.set_body(json_str)

    result = send_msg_ob.send_msg(msg_a, msg_b)
    response.status = result["statusCode"]

    return result

@post('/users') #creating a user
def create_user():
    ct = request.get_header('content-type')
    if ct != 'application/json':
        return abort(response, 400, [
            "request content-type unacceptable:  body must be "
            "'application/json' (was '{0}')".format(ct)])

    scheme = request.urlparts.scheme
    netloc = request.urlparts.netloc

    id = request.json["id"] # In JSON, id is already an integer
    name = request.json["name"]
    msgDict = {
        OP: 'create_user',
        ARGS: {
            'name' : name,
            'id' : id,
            'links': {
                'self': "{0}://{1}/users/{2}".format(scheme, netloc, id)
            }
        }
    }

    return convert_and_send(msgDict)

@get('/users') #get all users
def get_all_users():
    print "Retrieving all users\n"

    msgDict = {OP: 'get_all_users', ARGS: {}}
    return convert_and_send(msgDict)

@get('/users/<id>') #get user by id
def get_user_by_id(id):
    id = int(id)
    print "Getting user with ID: {0}\n".format(id)

    msgDict = {OP: 'get_user_by_id', ARGS: {'id': int(id)}}
    return convert_and_send(msgDict)

@get('/names/<name>') #get user by name
def get_name_route(name):
    print "Retrieving name {0}\n".format(name)

    msgDict = {OP: 'get_user_by_name', ARGS: {'name': name}}
    return convert_and_send(msgDict)

@delete('/users/<id>') #delete user by id
def delete_user_by_id(id):
    id = int(id)
    print "Deleting user with ID: {0}\n".format(id)

    msgDict = {OP: 'delete_user_by_id', ARGS: {'id': int(id)}}
    return convert_and_send(msgDict)

@put('/users/<id>/activities/<activity>') #add activity for user
def add_activity(id, activity):
    id = int(id)
    print "Adding activity for id {0}, activity {1}\n".format(id, activity)

    msgDict = {OP: 'add_activity', ARGS: {'id': int(id), 'activity': activity}}
    return convert_and_send(msgDict)

@delete('/users/<id>/activities/<activity>') #delete activity for user
def delete_activity_route(id, activity):
    id = int(id)
    print "Deleting activity for id {0}, activity {1}\n".format(id, activity)

    msgDict = {OP: 'delete_activity', ARGS: {'id': int(id), 'activity': activity}}
    return convert_and_send(msgDict)

@delete('/names/<name>') #delete user by name
def delete_user_by_name(name):
    print "Deleting name {0}\n".format(name)

    msgDict = {OP: 'delete_by_name', ARGS: {'name': name}}
    return convert_and_send(msgDict)

'''
   Boilerplate: Do not modify the following function. It
   is called by frontend.py to inject the names of the two
   routines you write in this module into the SendMsg
   object.  See the comments in SendMsg.py for why
   we need to use this awkward construction.

   This function creates the global object send_msg_ob.

   To send messages to the two backend instances, call

       send_msg_ob.send_msg(msg_a, msg_b)

   where

       msg_a is the boto.message.Message() you wish to send to a3_in_a.
       msg_b is the boto.message.Message() you wish to send to a3_in_b.

       These must be *distinct objects*. Their contents should be identical.
'''
def set_send_msg(send_msg_ob_p):
    global send_msg_ob
    send_msg_ob = send_msg_ob_p.setup(write_to_queues, set_dup_DS)

'''
   EXTEND:
   Set up the input queues and output queue here
   The output queue reference must be stored in the variable q_out
'''

conn = boto.sqs.connect_to_region(AWS_REGION) #SQS Connection

a3_in_a = conn.create_queue(Q_IN_NAME_BASE + "_a") #queue_in_a
a3_in_b = conn.create_queue(Q_IN_NAME_BASE + "_b") #queue_in_b

q_out = conn.create_queue(Q_OUT_NAME) #output queue


def write_to_queues(msg_a, msg_b):
    # EXTEND:
    # Send msg_a to a3_in_a and msg-b to a3_in_b
    a3_in_a.write(msg_a)
    a3_in_b.write(msg_b)

'''
    EXTEND:
    Manage the data structures for detecting the first and second
    responses and any duplicate responses.
'''

# Define any necessary data structures globally here
idMap = {}
COUNT = 'count' #How many times the response for a particular msg id has been received
ASYNC_RES = 'async_res' #The async result associated with request

def is_first_response(id):
    # EXTEND:
    # Return True if this message is the first response to a request
    try:
        return idMap[id][COUNT] == 0
    except KeyError:
        return False

def is_second_response(id):
    # EXTEND:
    # Return True if this message is the second response to a request
    try:
        return idMap[id][COUNT] == 1
    except KeyError:
        return False

def get_response_action(id):
    # EXTEND:
    # Return the action for this message
    return idMap[id][ASYNC_RES]

def get_partner_response(id):
    # EXTEND:
    # Return the id of the partner for this message, if any
    pass

def mark_first_response(id):
    # EXTEND:
    # Update the data structures to note that the first response has been received
    idMap[id][COUNT] = 1

def mark_second_response(id):
    # EXTEND:
    # Update the data structures to note that the second response has been received
    idMap[id][COUNT] = 2

def clear_duplicate_response(id):
    # EXTEND:
    # Do anything necessary (if at all) when a duplicate response has been received
    pass

def set_dup_DS(action, sent_a, sent_b):
    dupDict = {COUNT: 0, ASYNC_RES: action}

    idMap[sent_a.id] = dupDict
    idMap[sent_b.id] = dupDict
    '''
       EXTEND:
       Set up the data structures to identify and detect duplicates
       action: The action to perform on receipt of the response.
               Opaque data type: Simply save it, do not interpret it.
       sent_a: The boto.sqs.message.Message() that was sent to a3_in_a.
       sent_b: The boto.sqs.message.Message() that was sent to a3_in_b.

               The .id field of each of these is the message ID assigned
               by SQS to each message.  These ids will be in the
               msg_id attribute of the JSON object returned by the
               response from the backend code that you write.
    '''
